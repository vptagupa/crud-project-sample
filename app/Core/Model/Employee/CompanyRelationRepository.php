<?php

namespace App\Core\Model\Employee;

use App\Core\Model\Employee\CompanyRelation;
use App\Core\Model\Employee\CompanyRelationId;

/**
* This is a Employee repository
* which contains Employee records and events
*/
interface CompanyRelationRepository
{
    /**
     * Get Relation
     *
     * @param App\Core\Model\Employee\CompanyRelationId $relationId
     *
     * @return CompanyRelatiApp\Core\Model\Employee\CompanyRelation || null
     */
    public function getCompanyRelation(CompanyRelationId $relationId): ?CompanyRelation;

    /**
     * Get List of Relations
     *
     *
     * @return array
     */
    public function getAll(): array;

}
