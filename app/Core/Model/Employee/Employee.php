<?php

namespace App\Core\Model\Employee;

use App\Core\Model\Employee\EmployeeName;
use App\Core\Model\Employee\EmployeeId;
use App\Core\Model\Company\Company;
use App\Infrastructure\Contract\Email;

/**
* This is a employee entity that defines its attributes
*/
interface Employee
{
    /**
     * Get employee id
     *
     * @return App\Core\Model\Employee\EmployeeId
     */
    public function id(): EmployeeId;

    /**
     * Get employee first name
     *
     * @return string
     */
    public function getFirstName(): string;

    /**
     * Get employee last name
     *
     * @return string
     */
    public function getLastName(): string;

    /**
     * Get employee full name
     *
     * @return string
     */
    public function getFullName(): string;

    /**
     * Get employee Company Relation
     *
     * @return App\Core\Model\Company\Company $company
     */
    public function company(): ?Company;

    /**
     * Get employee email
     *
     * @return string
     */
    public function getEmail(): ?string;

    /**
     * Get employee phone
     *
     * @return string
     */
    public function getPhone(): ?string;

    /**
     * Change employee name
     *
     * @param App\Core\Model\Employee\EmployeeName $name
     *
     * @return void
     */
    public function changeName(EmployeeName $name): void;

    /**
     * Change company
     *
     * @param App\Core\Model\Company\Company $company
     *
     * @return void
     */
    public function changeCompany(Company $company): void;

    /**
     * Change employee email
     *
     * @param App\Infrastructure\Contract\Email $email
     *
     * @return void
     */
    public function changeEmail(Email $email): void;

    /**
     * Change employee phone
     *
     * @param String $phone
     *
     * @return void
     */
    public function changePhone(?string $phone = ''): void;
}
