<?php

namespace App\Infrastructure\Application;

use App\Core\Application\EmployeeService as EmployeeServiceInterface;
use App\Core\Model\Employee\EmployeeRepository as EmployeeRepositoryInterface;

use App\Infrastructure\Model\Employee\Employee;
use App\Infrastructure\Model\Employee\EmployeeName;

use App\Core\Model\Employee\Employee as EmployeeInterface;
use App\Core\Model\Employee\EmployeeName as EmployeeNameInterface;
use App\Core\Model\Employee\EmployeeId;
use App\Infrastructure\Contract\Email as EmailInterface;
use App\Infrastructure\Contract\Image;
use App\Infrastructure\Common\Curl;
use App\Infrastructure\Common\Email;
use App\Infrastructure\Contract\DataPager;

/**
* This service will validate and
* create, update, delete and retrieve employee records
*/
final class EmployeeService implements EmployeeServiceInterface
{
    public function __construct(
        EmployeeRepositoryInterface $employeeRepository
    ) {
        $this->employeeRepository = $employeeRepository;
    }
    /**
     * This will validate employee record and create
     * otherwise the invalid exception is thrown
     *
     * @param App\Core\Model\Employee\EmployeeName $name
     * @param App\Infrastructure\Contract\Email $email
     * @param string $phone
     *
     * @throws exception
     * @return void
     */
    public function create(
        EmployeeNameInterface $name,
        EmailInterface $email,
        ?string $phone = ''
    ): void
    {
        // Create employee record
        $this->employeeRepository->createEmployee(
            new Employee(
                null,
                $name,
                null,
                $email,
                $phone
            )
        );
    }

    /**
     * This will update the employee record
     * If only the employee id is existed in the database
     * and its attributes are valid
     * otherwise the error exception will be thrown
     *
     * @param App\Core\Model\Employee\EmployeeId $employeeId
     * @param App\Core\Model\Employee\EmployeeName $name
     * @param App\Infrastructure\Contract\Email $email
     *
     * @throws exception
     * @return void
     */
    public function update(
        EmployeeId $employeeId,
        EmployeeNameInterface $name,
        EmailInterface $email,
        ?string $phone = ''
    ): void
    {
        // If the employee did not exist exception will be thrown
        $employee = $this->employeeRepository->getEmployee($employeeId);
        if (! $employee) {
            throw new \Exception("Failed to update. Employee could not be found!");
        }
        // Change employee record
        $employee->changeName($name);
        $employee->changeEmail($email);
        $employee->changePhone($phone);

        // Update and commit changes of employee record
        $this->employeeRepository->updateEmployee($employee);
    }

    /**
     * This will delete the employee record
     * If only the employee id is existed in the database
     * otherwise the error exception will be thrown
     *
     * @param App\Core\Model\Employee\EmployeeId $employeeId
     *
     * @throws exception
     * @return bool
     */
    public function delete(EmployeeId $employeeId): bool
    {
        // If the employee did not exist exception will be thrown
        $employee = $this->employeeRepository->getEmployee($employeeId);
        if (! $employee) {
            throw new \Exception("Failed to delate. Employee could not be found!");
        }
        // Delete employee
        return $this->employeeRepository->deleteEmployee($employeeId);
    }

    /**
     * Listing of the employees with limit and offset and a given criteria
     *
     * @throws exception
     * @return void
     */
    public function listEmployee(
        array $likeCriteria = array(),
        int $limit = 10,
        int $offset = 0
    ): DataPager {
        // Get list of employees
        return $this->employeeRepository->getEmployees(
            $likeCriteria,
            $limit,
            $offset
        );
    }

    /**
     * Get Employee Record by a given id
     * Thrown an error exception if the employee id did not exist in the database
     *
     * @param App\Core\Model\Employee\EmployeeId $employeeId
     *
     * @throws exception
     * @return void
     */
    public function getEmployee(EmployeeId $employeeId): EmployeeInterface
    {
        // If the employee did not exist exception will be thrown
        $employee = $this->employeeRepository->getEmployee($employeeId);
        if (! $employee) {
            throw new \Exception("Failed to retrieve. Employee could not be found!");
        }

        return $employee;
    }

    /**
     * Import List of Users from the api
     * Thrown an exception if the employee name
     * already existing in the database
     *
     * @param string $source url
     *
     * @throws exception
     * @return void
     */
    public function importImployeeFromApiUrl(string $sourceUrl): void
    {
        // Convert source api to array format
        // Import to employees table, the list of users
        // Employee Name and Email should be unique and
        // Exception will be thrown if error occured
        $curl = new Curl($sourceUrl);
        $employees = json_decode($curl->get());
        foreach($employees as $employee) {
            $employee = is_array($employee) ? (object)$employee : $employee;
            $name = explode(" ",$employee->name);
            $this->create(
                new EmployeeName($name[0], $name[1] ?? ''),
                new Email($employee->email),
                $employee->phone
            );
        }
    }
}
