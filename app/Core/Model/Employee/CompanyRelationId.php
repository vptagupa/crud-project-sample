<?php

namespace App\Core\Model\Employee;

/**
* This is a Company Relation id as a value object
*/
interface CompanyRelationId
{
    /**
     * Get relation id
     *
     * @return int
     */
    public function get(): int;
}
