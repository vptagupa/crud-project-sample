require("./bootstrap");

require("izitoast/dist/css/iziToast.css");

window.Vue = require("vue");
window.izitoast = require("izitoast");

Vue.component(
    "employee-table-component",
    require("./component/employee/table.vue").default
);

Vue.component(
    "company-table-component",
    require("./component/company/table.vue").default
);

Vue.component(
    "upload-employee-component",
    require("./component/upload-employee.vue").default
);

Vue.component("login-component", require("./component/login.vue").default);

const app = new Vue({
    el: "#root"
});
