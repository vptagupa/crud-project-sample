<?php

namespace App\Core\Model\Company;

/**
* This is a Company Name as a value object
* This will protect the company name on its desired value
*/
interface CompanyName
{

    /**
     * Get employee Name
     *
     * @return string
     */
    public function get(): string;
}
