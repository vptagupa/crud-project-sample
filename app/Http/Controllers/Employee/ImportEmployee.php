<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Core\Application\EmployeeService;
use App\Infrastructure\Common\Email;
use App\Infrastructure\Common\Curl;
use App\Infrastructure\Model\Employee\EmployeeName;

class ImportEmployee extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Add Employee Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the adding of the employee,
    | it can return success or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\EmployeeService $service
     *
     * @return void
     */
     public function __construct(EmployeeService $service)
     {
         $this->service = $service;
     }

     /**
     *
     * Handle Importation of Employees from the api
     *
     * @return Illuminate\Http\Response
     */
    public function handle(Request $request)
    {
        try
        {
            // Upload list of users to employee tables
            $this->service->importImployeeFromApiUrl($request->get('source'));

            return response(array(
                'success' => true,
                'message' => 'Successfuly Imported Users!'
            ));
        } catch (\Exception $e) {
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
