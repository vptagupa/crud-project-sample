@extends('layout.admin')

@section('title','Company')

@section('header')

@endsection
@section('content')
<div class="alert alert-primary" role="alert">
  Create, Update, Delete and Search Company Records
</div>
<company-table-component base-url="{{url("/")}}"></company-table-component>
@endsection
