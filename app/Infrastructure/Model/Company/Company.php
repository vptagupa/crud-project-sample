<?php

namespace App\Infrastructure\Model\Company;

use App\Core\Model\Company\Company as CompanyInterface;

use App\Core\Model\Company\CompanyName;
use App\Core\Model\Company\CompanyId;
use App\Infrastructure\Contract\Image;
use App\Infrastructure\Contract\Email;

/**
* This is a company class that defines its attributes
*/
class Company implements CompanyInterface
{
    /**
    * Employee phone
    *
    * @var string
    */
    private $phone;

    /**
     * Initiate employee
     *
     * @param App\Core\Model\Company\CompanyId $id
     * @param App\Core\Model\Company\CompanyName $name
     * @param App\Infrastructure\Contract\Email $email
     * @param string $logo
     *
     * @return void
     */
    public function __construct(
        ?CompanyId $id,
        CompanyName $name,
        Email $email,
        string $logo = '',
        array $employees
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->logo = $logo;
        $this->employees = $employees;
    }

    /**
     * Get company id
     *
     * @return App\Core\Model\Company\CompanyId
     */
    public function id(): CompanyId
    {
        return $this->id;
    }

    /**
     * Get company name
     *
     * @return string
     */
    public function name(): CompanyName
    {
        return $this->name;
    }

    /**
     * Get company email
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email->get();
    }

    /**
     * Get company employees
     *
     * @return string
     */
    public function getEmployees(): array
    {
        return $this->employees;
    }

    /**
     * Get company logo
     *
     * @return string
     */
    public function getLogo(): string
    {
        if ($this->logo instanceOf Image) {
            return $this->logo->getPublicUrl();
        }
        return $this->logo;
    }

    /**
     * Change company name
     *
     * @param App\Core\Model\Company\CompanyName $name
     *
     * @return void
     */
    public function changeName(CompanyName $name): void
    {
        $this->name = $name;
    }

    /**
     * Change company email
     *
     * @param Email $email
     *
     * @return void
     */
    public function changeEmail(Email $email): void
    {
        $this->email = $email;
    }

    /**
     * Change company logo
     *
     * @param string $logo
     *
     * @return void
     */
    public function changeLogo(string $logo): void
    {
        $this->logo = $logo;
    }
}
