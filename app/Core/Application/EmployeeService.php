<?php

namespace App\Core\Application;

use App\Core\Model\Employee\EmployeeName;
use App\Core\Model\Employee\Employee;
use App\Core\Model\Employee\EmployeeId;
use App\Infrastructure\Contract\Email;
use App\Infrastructure\Contract\Image;
use App\Infrastructure\Contract\DataPager;

/**
* This service will validate and
* create, update, delete and retrieve employee records
*/
interface EmployeeService
{
    /**
     * This will validate employee record and create
     * otherwise the invalid exception is thrown
     *
     * @param App\Core\Model\Employee\EmployeeName $name
     * @param App\Infrastructure\Contract\Email $email
     * @param string $phone
     *
     * @throws exception
     * @return void
     */
    public function create(
        EmployeeName $name,
        Email $email,
        string $phone
    ): void;

    /**
     * This will update the employee record
     * If only the employee id is existed in the database
     * and its attributes are valid
     * otherwise the error exception will be thrown
     *
     * @param App\Core\Model\Employee\EmployeeId $employeeId
     * @param App\Core\Model\Employee\EmployeeName $name
     * @param App\Infrastructure\Contract\Email $email
     *
     * @throws exception
     * @return void
     */
    public function update(
        EmployeeId $employeeId,
        EmployeeName $name,
        Email $email,
        string $phone
    ): void;

    /**
     * This will delete the employee record
     * If only the employee id is existed in the database
     * otherwise the error exception will be thrown
     *
     * @param App\Core\Model\Employee\EmployeeId $employeeId
     *
     * @throws exception
     * @return bool
     */
    public function delete(EmployeeId $employeeId): bool;

    /**
     * Listing of the employees with limit and offset and a given criteria
     *
     * @param array $likeCriteria
     * @param int $limit
     * @param int $offset
     *
     * @throws exception
     * @return void
     */
    public function listEmployee(
        array $likeCriteria = array(),
        int $limit = 10,
        int $offset = 0
    ): DataPager;

    /**
     * Get Employee Record by a given id
     * Thrown an error exception if the employee id did not exist in the database
     *
     * @param App\Core\Model\Employee\EmployeeId $employeeId
     *
     * @throws exception
     * @return void
     */
    public function getEmployee(EmployeeId $employeeId): Employee;

    /**
    * Import List of Users from the api
    * Thrown an exception if the employee name
    * already existing in the database
    *
    * @param string $source url
    *
    * @throws exception
    * @return void
    */
    public function importImployeeFromApiUrl(string $sourceUrl): void;
}
