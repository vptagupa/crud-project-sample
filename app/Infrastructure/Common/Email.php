<?php

namespace App\Infrastructure\Common;

use App\Infrastructure\Contract\Email as EmailInterface;

class Email implements EmailInterface
{
    /**
    *
    * var string
    */
    private $email;

    public function __construct($email = '') {
        $this->email = $email;

        if (!empty($this->email)
            && !filter_var($this->email, FILTER_VALIDATE_EMAIL)
        ) {
            throw new \Exception("Invalid email address");
        }
    }

    public function get()
    {
        return $this->email;
    }

    public function __toString()
    {
        return $this->email;
    }

}
