@extends('layout.admin')

@section('title','Employee')

@section('header')

@endsection
@section('content')
<div class="alert alert-primary" role="alert">
  Create, Update, Delete and Search Employee Records
</div>
<employee-table-component base-url="{{url("/")}}"></employee-table-component>
@endsection
