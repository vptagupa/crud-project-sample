<?php

namespace App\Core\Model\Employee;

/**
* This is a Employee id as a value object
*/
interface EmployeeId
{
    /**
     * Get employee id
     *
     * @return int
     */
    public function get(): int;
}
