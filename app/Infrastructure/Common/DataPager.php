<?php

namespace App\Infrastructure\Common;

use App\Infrastructure\Contract\DataPager as DataPagerInterface;
use Illuminate\Support\Collection;

/**
* This class is for the pagination purposes
*/
class DataPager implements DataPagerInterface
{
     /**
     *
     * var int
     */
     private $total_records = 0;

     /**
     *
     * var array
     */
     private $data = array();

     /**
     *
     * Initialize
     * @param int $totalRecords
     * @param Illuminate\Support\Collection $data
     */
     public function __construct(int $totalRecords, Collection $data)
     {
         $this->total_records = $totalRecords;
         $this->data = $data;
     }

     /**
     *
     * Get Total Records
     * @return int
     */
     public function getTotalRecords(): int
     {
         return count($this->get()) > 0 ? $this->total_records : 0;
     }

     /**
     *
     * Get Records
     * @return int
     */
     public function get(): array
     {
         return $this->data->all();
     }
 }
