<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;

use App\Core\Application\CompanyService;
use App\Infrastructure\Model\Company\CompanyId;
use App\Core\Model\Employee\Employee;

class GetCompany extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Get Company Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the retrieving of the company records,
    | it can return success or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\CompanyService $service
     *
     * @return void
     */
     public function __construct(CompanyService $service)
     {
         $this->service = $service;
     }

     /**
     *
     * Handle Getting of company record
     *
     * @return Illuminate\Http\Response
     */
    public function handle(int $id)
    {
        try
        {
            // Get company record otherwise exception is thrown
            $company = $this->service->getCompany(
                new CompanyId($id)
            );

            return response(array(
                'success' => true,
                'data' => array(
                    'id' => $company->id()->get(),
                    'name' => $company->name()->get(),
                    'email' => $company->getEmail(),
                    'logo' => route('company.logo.show',$company->getLogo().'?t='.rand(1,99999)),
                    'employees' => array_map( function(Employee $employee) {
                        return array(
                            'id' => $employee->id()->get(),
                            'name' => $employee->getFullName(),
                            'email' => $employee->getEmail(),
                            'phone' => $employee->getPhone(),
                            'photo' => route('employee.photo.show', $this->randomPhoto())
                        );
                    }, $company->getEmployees())
                )
            ));
        } catch (\Exception $e) {
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    private function randomPhoto()
    {
        $photos =  array(
            'default.jpg',
            'jimrohn.jpg',
            'tonyrobbins.png',
            'melrobbins.jpg'
        );
        return $photos[rand(0, 3)];
    }
}
