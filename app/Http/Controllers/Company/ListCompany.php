<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Core\Application\CompanyService;
use App\Core\Model\Company\Company;
use App\Infrastructure\Model\Company\CompanyId;

class ListCompany extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | List Company Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the listing of the companies,
    | it can return success or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\CompanyService $service
     *
     * @return void
     */
     public function __construct(CompanyService $service)
     {
         $this->service = $service;
     }

     /**
     *
     * Handle Getting list of companies
     *
     * @return Illuminate\Http\Response
     */
    public function handle(Request $request)
    {
        try
        {
            // List companies with pagination and search criteria
            $companies = $this->service->listCompany(
                $request->get('criteria'),
                $request->get('limit'),
                ($request->get('page') - 1) * $request->get('limit')
            );

            return response(array(
                'success' => true,
                'total_rows' => $companies->getTotalRecords(),
                'data' => array_map( function(Company $company) {
                    return array(
                        'id' => $company->id()->get(),
                        'name' => $company->name()->get(),
                        'email' => $company->getEmail(),
                        'logo' => route('company.logo.show',$company->getLogo().'?t='.rand(1,99999))
                    );
                }, $companies->get())
            ));
        } catch (\Exception $e) {            
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
