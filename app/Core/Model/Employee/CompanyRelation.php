<?php

namespace App\Core\Model\Employee;

use App\Core\Model\Employee\CompanyRelationId;

/**
* This is a company relation that defines its attributes
*/
interface CompanyRelation
{
    /**
     * Get relation id
     *
     * @return string
     */
    public function id(): CompanyRelationId;

    /**
     * Get relation name
     *
     * @return string
     */
    public function getName(): string;
}
