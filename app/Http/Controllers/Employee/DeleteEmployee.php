<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;

use App\Core\Application\EmployeeService;
use App\Infrastructure\Model\Employee\EmployeeId;

class DeleteEmployee extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Delete Employee Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the deletion of the employee,
    | it can return success or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\EmployeeService $service
     *
     * @return void
     */
     public function __construct(EmployeeService $service)
     {
         $this->service = $service;
     }

     /**
     *
     * Handle Deletion of employee record
     *
     * @return Illuminate\Http\Response
     */
    public function handle(int $id)
    {
        try
        {
            // Delete employee record otherwise exception is thrown
            $this->service->delete(
                new EmployeeId($id)
            );

            return response(array(
                'success' => true,
                'message' => 'Successfuly Deleted!'
            ));
        } catch (\Exception $e) {

            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
