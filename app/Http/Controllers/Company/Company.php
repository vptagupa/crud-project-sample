<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;


class Company extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | View Company Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles of the company page,
    |
    */

    /**
     * Create a new controller instance.
     *
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth');
     }

     public function index()
     {
         return view('company.index');
     }
}
