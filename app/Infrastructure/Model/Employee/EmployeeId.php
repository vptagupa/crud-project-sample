<?php

namespace App\Infrastructure\Model\Employee;

use App\Core\Model\Employee\EmployeeId as EmployeeIdInterface;

/**
* This is a Employee id as a value object
*/
class EmployeeId implements EmployeeIdInterface
{
    /**
    *  id
    * @var $id
    */
    private $id;

    /**
    * Initiate id
    * @param int $id
    */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get employee id
     *
     * @return int
     */
    public function get(): int
    {
        return $this->id;
    }
}
