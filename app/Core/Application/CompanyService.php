<?php

namespace App\Core\Application;

use App\Core\Model\Company\Company;
use App\Core\Model\Company\CompanyName;
use App\Core\Model\Company\CompanyId;
use App\Infrastructure\Contract\Email;
use App\Infrastructure\Contract\Image;
use App\Infrastructure\Contract\DataPager;

/**
* This service will validate, retrieve, create, update, delete and list companies
*/
interface CompanyService
{
    /**
     * This will validate company record and create
     * otherwise the invalid exception is thrown
     *
     * @param App\Core\Model\Company\CompanyName $name
     * @param App\Infrastructure\Contract\Email $email
     * @param App\Infrastructure\Contract\Image $logo
     * @param array $employees
     *
     * @throws exception
     * @return void
     */
    public function create(
        CompanyName $name,
        Email $email,
        Image $logo,
        array $employees
        ): void;

    /**
     * This will update the company record
     * If only the company id is existed in the database
     * and its attributes are valid
     * otherwise the error exception will be thrown
     *
     * @param App\Core\Model\Company\CompanyId $companyId
     * @param App\Core\Model\Company\CompanyName $name
     * @param App\Infrastructure\Contract\Email $email
     * @param App\Infrastructure\Contract\Image $logo
     * @param array $employees
     *
     * @throws exception
     * @return void
     */
    public function update(
        CompanyId $companyId,
        CompanyName $name,
        Email $email,
        Image $logo,
        array $employees
    ): void;

    /**
     * This will delete the company record
     * If only the company id is existed in the database
     * otherwise the error exception will be thrown
     *
     * @param App\Core\Model\Company\CompanyId $companyId
     *
     * @throws exception
     * @return bool
     */
    public function delete(CompanyId $companyId): bool;

    /**
     * Listing of the companies with limit and offset and a given criteria
     *
     * @param array $likeCriteria
     * @param int $limit
     * @param int $offset
     *
     * @throws exception
     * @return App\Infrastructure\Contract\DataPager
     */
    public function listCompany(
        array $likeCriteria = array(),
        int $limit = 10,
        int $offset = 0
    ): DataPager;

    /**
     * Get Company Record by a given id
     * Thrown an error exception
     * if the company id did not exist in the database
     *
     ** @param App\Core\Model\Company\CompanyId $companyId
     *
     * @throws exception
     * @return App\Core\Model\Company\Company
     */
    public function getCompany(CompanyId $companyId): ?Company;
}
