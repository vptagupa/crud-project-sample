<?php

namespace App\Core\Model\Company;

use App\Core\Model\Company\Company;
use App\Core\Model\Company\CompanyId;
use App\Infrastructure\Contract\DataPager;

/**
* This is a company repository
* which contains company records and events
*/
interface CompanyRepository
{
    /**
     * Create Company
     *
     * @param App\Core\Model\Company\Company $company
     *
     * @return void
     */
    public function createCompany(Company $company): Company;

    /**
     * Update Company
     *
     * @param App\Core\Model\Company\Company $company
     *
     * @return void
     */
    public function updateCompany(Company $company): void;

    /**
     * Delete Company
     *
     * @param App\Core\Model\Company\CompanyId $companyId
     *
     * @return bool
     */
    public function deleteCompany(CompanyId $companyId): bool;

    /**
     * Get company record
     * By a given id
     *
    * @param App\Core\Model\Company\CompanyId $companyId
     *
     * @return void
     */
    public function getCompany(CompanyId $companyId): ?Company;

    /**
     * Get List of Companies with limit and offset
     * By a given like criteria
     *
     * @param array $likeCriteria
     * @param int $limit
     * @param int $offset
     *
     * @return App\Infrastructure\Contract\DataPager
     */
    public function getCompanies(
        array $likeCriteria = array(),
        int $limit = 10,
        int $offset = 0
    ): DataPager;
}
