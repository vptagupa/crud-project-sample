<?php

namespace App\Core\Model\Employee;

/**
* This is a Employee Name as a value object
* This will protect the Employee name in its desired value
*/
interface EmployeeName
{
    /**
     * Get employee Full Name
     *
     * @return string
     */
    public function getFullName(): string;

    /**
     * Get employee First Name
     *
     * @return string
     */
    public function getFirstName(): string;

    /**
     * Get employee Last Name
     *
     * @return string
     */
    public function getLastName(): string;
}
