<?php

namespace App\Infrastructure\Model\Company;

use App\Core\Model\Company\CompanyId as CompanyIdInterface;

/**
* This is a Company id as a value object
*/
class CompanyId implements CompanyIdInterface
{
    /**
    *  id
    * @var $id
    */
    private $id;

    /**
    * Initiate id
    * @param int $id
    */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get employee id
     *
     * @return int
     */
    public function get(): int
    {
        return $this->id;
    }
}
