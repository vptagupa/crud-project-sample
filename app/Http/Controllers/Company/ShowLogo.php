<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use File;
use Response;

class ShowLogo extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Show Company Logo Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the retrieving of the company logo,
    | it can return success or error whatever the outcome of the service
    |
    */


    public function handle(string $filename)
    {
       if (!File::exists($path = storage_path('app/logos/' . $filename))) {
           $path = storage_path('app/logo-default.png');
       }

       $file = File::get($path);
       $type = File::mimeType($path);

       $response = Response::make($file, 200);
       $response->header("Content-Type", $type);

       return $response;
    }
}
