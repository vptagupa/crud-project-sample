<?php

namespace App\Infrastructure\Common;

use App\Infrastructure\Contract\Image as ImageInterface;

class Image implements ImageInterface
{
    /**
    *
    * var mixed
    */
    private $image;

    public function __construct($image = '') {
        $this->image = $image;
    }

    /**
    * Get Image
    *
    * @return mixed
    */
    public function get()
    {
        return $this->image;
    }

    /**
    * Get Image Type
    *
    * @return string
    */
    public function getType(): string
    {
        return $this->image->clientExtension();
    }

    /**
    * Get Image Size
    *
    * @return float
    */
    public function getSize(): float
    {
        return $this->image->getSize() / 1000000; // converted to MegaByte;
    }

    /**
    * Get Image validation status
    *
    * @return bool
    */
    public function isValid(): bool
    {
        return $this->image->isValid();
    }

    /**
    * Get Image Validation error
    *
    * @return string
    */
    public function getError(): string
    {
        return $this->image->getErrorMessage();
    }

    /**
    * Store image
    *
    * @param string $path
    * @param string $filename
    *
    * @return string
    */
    public function storeAs(string $path, string $filename): string
    {
        return $this->image->storeAs($path, $filename);
    }

    /**
    * Is not empty?
    *
    * @return bool
    */
    public function notEmpty(): bool
    {
        return !empty($this->image);
    }
}
