<?php

namespace App\Infrastructure\Contract;

interface Image
{
    /**
    * Get Image
    *
    * @return mixed
    */
    public function get();

    /**
    * Get Image Type
    *
    * @return string
    */
    public function getType(): string;

    /**
    * Get Image Size
    *
    * @return float
    */
    public function getSize(): float;

    /**
    * Get Image validation status
    *
    * @return bool
    */
    public function isValid(): bool;

    /**
    * Get Image Validation error
    *
    * @return string
    */
    public function getError(): string;

    /**
    * Store image
    *
    * @param string $path
    * @param string $filename
    *
    * @return string
    */
    public function storeAs(string $path, string $filename): string;

    /**
    * Is not empty?
    *
    * @return bool
    */
    public function notEmpty(): bool;
 }
