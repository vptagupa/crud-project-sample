<?php

namespace App\Infrastructure\Model\Employee;

use App\Core\Model\Employee\Employee as EmployeeInterface;
use App\Core\Model\Employee\EmployeeName;
use App\Core\Model\Employee\EmployeeId;
use App\Core\Model\Company\Company;
use App\Infrastructure\Contract\Email;

/**
* This is a employee class that defines its attributes
*/
class Employee implements EmployeeInterface
{
    /**
    * Employee Id
    *
    * @var App\Core\Model\Employee\EmployeeId
    */
    private $id;

    /**
    * Employee Name
    *
    * @var App\Core\Model\Employee\EmployeeName
    */
    private $name;

    /**
    * Employee Company Relation
    *
    * @var App\Core\Model\Employee\CompanyRelation
    */
    private $companyRelation;

    /**
    * Employee Email
    *
    * @var string
    */
    private $email;

    /**
    * Employee phone
    *
    * @var string
    */
    private $phone;

    /**
     * Initiate employee
     *
     * @param App\Core\Model\Employee\EmployeeId $id
     * @param App\Core\Model\Employee\EmployeeName $name
     * @param App\Core\Model\Company\Company $company
     * @param App\Infrastructure\Contract\Email $email
     * @param string $phone
     *
     * @return void
     */
    public function __construct(
        ?EmployeeId $id,
        EmployeeName $name,
        ?Company $company,
        Email $email,
        ?string $phone
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->company = $company;
        $this->email = $email;
        $this->phone = $phone;
    }

    /**
     * Get employee id
     *
     * @return App\Core\Model\Employee\EmployeeId
     */
    public function id(): EmployeeId
    {
        return $this->id;
    }

    /**
     * Get employee first name
     *
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->name->getFirstName();
    }

    /**
     * Get employee last name
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->name->getLastName();
    }

    /**
     * Get employee full name
     *
     * @return string
     */
    public function getFullName(): string
    {
        return $this->name->getFullName();
    }

    /**
     * Get employee Company Relation
     *
     * @return App\Core\Model\Company\Company $company
     */
    public function company(): ?Company
    {
        return $this->company;
    }

    /**
     * Get employee email
     *
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email->get();
    }

    /**
     * Get employee phone
     *
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Change employee name
     *
     * @param EmployeeName $name
     *
     * @return void
     */
    public function changeName(EmployeeName $name): void
    {
        $this->name = $name;
    }

    /**
     * Change company relation
     *
     * @param App\Core\Model\Company\Company $company
     *
     * @return void
     */
    public function changeCompany(Company $company): void
    {
        $this->company = $company;
    }

    /**
     * Change employee email
     *
     * @param App\Infrastructure\Contract\Email $email
     *
     * @return void
     */
    public function changeEmail(Email $email): void
    {
        $this->email = $email;
    }

    /**
     * Change employee phone
     *
     * @param String $phone
     *
     * @return void
     */
    public function changePhone(?string $phone = ''): void
    {
        $this->phone = $phone;
    }
}
