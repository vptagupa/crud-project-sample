<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;

use App\Core\Application\CompanyService;
use App\Infrastructure\Model\Company\CompanyId;

class DeleteCompany extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Delete Company Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the deletion of the company,
    | it can return success or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\CompanyService $service
     *
     * @return void
     */
     public function __construct(CompanyService $service)
     {
         $this->service = $service;
     }

     /**
     *
     * Handle Deletion of company
     *
     * @return Illuminate\Http\Response
     */
    public function handle(int $id)
    {
        try
        {
            // Delete company otherwise exception is thrown
            $this->service->delete(
                new CompanyId($id)
            );

            return response(array(
                'success' => true,
                'message' => 'Successfuly Deleted!'
            ));
        } catch (\Exception $e) {
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
