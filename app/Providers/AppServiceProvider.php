<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
        * Bind Employee Service
        */
        $this->app->bind('App\Core\Application\EmployeeService', function ($app) {
            return new \App\Infrastructure\Application\EmployeeService(
                new \App\Infrastructure\Model\Employee\EmployeeRepository
            );
        });

        /*
        * Bind Company Service
        */
        $this->app->bind('App\Core\Application\CompanyService', function ($app) {
            return new \App\Infrastructure\Application\CompanyService(
                new \App\Infrastructure\Model\Company\CompanyRepository,
                new \App\Infrastructure\Model\Employee\EmployeeRepository
            );
        });
    }
}
