<?php

namespace App\Infrastructure\Application;

use App\Core\Application\CompanyService as CompanyServiceInterface;
use App\Core\Model\Company\CompanyRepository;
use App\Core\Model\Employee\EmployeeRepository;
use App\Core\Model\Company\Company as CompanyInterface;
use App\Infrastructure\Model\Company\Company;
use App\Core\Model\Company\CompanyName;
use App\Core\Model\Company\CompanyId;
use App\Core\Model\Employee\Employee;
use App\Core\Model\Employee\EmployeeId;
use App\Infrastructure\Contract\Email;
use App\Infrastructure\Contract\Image as ImageInterface;
use App\Infrastructure\Common\Image;
use App\Infrastructure\Contract\DataPager;
use Storage;

/**
* This service will validate, retrieve, create, update, delete and list companies
*/
final class CompanyService implements CompanyServiceInterface
{
    public function __construct(
        CompanyRepository $companyRepository,
        EmployeeRepository $employeeRepository
    ) {
        $this->companyRepository = $companyRepository;
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * This will validate company record and create
     * otherwise the invalid exception is thrown
     *
     * @param App\Core\Model\Company\CompanyName $name
     * @param App\Infrastructure\Contract\Email $email
     * @param App\Infrastructure\Contract\Image $logo
     * @param array $employees
     *
     * @throws exception
     * @return void
     */
    public function create(
        CompanyName $name,
        Email $email,
        ImageInterface $logo,
        array $employees
    ): void {
        // Create company and upload logo if not empty
        $filename = $logo->notEmpty() ? $name->get().".".$logo->getType() : "default.jpg";
        $company = $this->companyRepository->createCompany(
            new Company(
                null,
                $name,
                $email,
                $filename,
                array()
            )
        );
        if ($logo->notEmpty()) {
            $logo->storeAs('logos', $filename);
        }

        // Assign employees to company
        $this->assignedEmployeeToCompany($company, $employees);
    }

    /**
     * This will update the company record
     * If only the company id is existed in the database
     * and its attributes are valid
     * otherwise the error exception will be thrown
     *
     * @param App\Core\Model\Company\CompanyId $companyId
     * @param App\Core\Model\Company\CompanyName $name
     * @param App\Infrastructure\Contract\Email $email
     * @param App\Infrastructure\Contract\Image $logo
     * @param array $employees
     *
     * @throws exception
     * @return void
     */
    public function update(
        CompanyId $companyId,
        CompanyName $name,
        Email $email,
        ImageInterface $logo,
        array $employees
    ): void
    {
        $filename = $logo->notEmpty() ? $name->get().".".$logo->getType() : "default.jpg";
        // Check company record if existing
        $company = $this->companyRepository->getCompany($companyId);
        if (! $company) {
            throw new \Exception("Failed to update. Company could not be found!");
        }
        // Change company record
        $company->changeName($name);
        $company->changeEmail($email);
        if ($logo->notEmpty()) {
            $company->changeLogo($filename);
        }
        // Update and commit changes of company record
        $this->companyRepository->updateCompany($company);
        if ($logo->notEmpty()) {
            $logo->storeAs('logos', $filename);
        }

        // Assign employees to company
        $this->assignedEmployeeToCompany($company, $employees);
    }

    /**
     * This will delete the company record
     * If only the company id is existed in the database
     * otherwise the error exception will be thrown
     *
     * @param App\Core\Model\Company\CompanyId $companyId
     *
     * @throws exception
     * @return void
     */
    public function delete(CompanyId $companyId): bool
    {
        // If the company did not exist exception will be thrown
        $company = $this->companyRepository->getCompany($companyId);
        if (! $company) {
            throw new \Exception("Failed to update. Company could not be found!");
        }
        // Delete Company
        return $this->companyRepository->deleteCompany($companyId);
    }

    /**
     * Listing of the companies with limit and offset and a given criteria
     *
     * @param array $likeCriteria
     * @param int $limit
     * @param int $offset
     *
     * @throws exception
     * @return App\Infrastructure\Contract\DataPager
     */
    public function listCompany(
        array $likeCriteria = array(),
        int $limit = 10,
        int $offset = 0
    ): DataPager {
        // Get list of company
        return $this->companyRepository->getCompanies(
            $likeCriteria,
            $limit,
            $offset
        );
    }

    /**
     * Get Company Record by a given id
     * Thrown an error exception
     * if the company id did not exist in the database
     *
     ** @param App\Core\Model\Company\CompanyId $companyId
     *
     * @throws exception
     * @return App\Core\Model\Company\Company
     */
    public function getCompany(CompanyId $companyId): ?CompanyInterface
    {
        // If the company did not exist exception will be thrown
        $company = $this->companyRepository->getCompany($companyId);
        if (! $company) {
            throw new \Exception("Failed to retrieve. Company could not be found!");
        }

        return $company;
    }

    /**
     * Assign employees to company
     * Assign only if employee id exist in the database
     *
     * @param App\Core\Model\Company\Company Company
     * @param array $employees
     *
     * @throws exception
     * @return void
     */
    private function assignedEmployeeToCompany(CompanyInterface $company, array $employees): void
    {
        // Assign and update each employee company relation
        $this->companyRepository->clearEmployees($company->id());
        array_map( function(EmployeeId $id) use ($company) {
            $employee = $this->employeeRepository->getEmployee($id);
            if ($employee) {
                $employee->changeCompany($company);
                $this->employeeRepository->updateEmployee($employee);
            }
        }, $employees);
    }
}
