<?php

namespace App\Infrastructure\Contract;

interface Email
{
    /**
    * Get Email
    *
    * @return mixed
    */
    public function get();

}
