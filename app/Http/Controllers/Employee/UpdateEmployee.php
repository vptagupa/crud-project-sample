<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Core\Application\EmployeeService;
use App\Infrastructure\Model\Employee\EmployeeId;
use App\Infrastructure\Model\Employee\EmployeeName;
use App\Infrastructure\Common\Email;

class UpdateEmployee extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Update Employee Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the updating of the employee record,
    | it can return success or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\EmployeeService $service
     *
     * @return void
     */
     public function __construct(EmployeeService $service, Request $request)
     {
         $this->service = $service;
         $this->request = $request;
     }

     /**
     *
     * Handle Update employee record
     *
     * @return Illuminate\Http\Response
     */
    public function handle(int $id)
    {
        try
        {
            // Validate form inputs
            $validated = $this->request->validate([
                'first_name' => ['required', 'max:35'],
                'last_name' => ['required', 'max:35']
            ]);

            // Update employee otherwise exception is thrown
            $this->service->update(
                new EmployeeId($id),
                new EmployeeName($validated['first_name'], $validated['last_name']),
                new Email($this->request->get('email')),
                $this->request->get('phone')
            );

            return response(array(
                'success' => true,
                'message' => 'Successfuly Update!'
            ));
        } catch (\Exception $e) {
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
