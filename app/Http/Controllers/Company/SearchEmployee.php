<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Core\Application\EmployeeService;
use App\Core\Model\Employee\Employee;

class SearchEmployee extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | List Employee Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the listing of the employees,
    | it can return success or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\EmployeeService $service
     *
     * @return void
     */
     public function __construct(EmployeeService $service)
     {
         $this->service = $service;
     }

     /**
     *
     * Handle Getting list of employee
     *
     * @return Illuminate\Http\Response
     */
    public function handle(string $name)
    {
        // Get list of employees
        $employees = $this->service->listEmployee(
            array('name' => $name),
            50,
            0
        );

        return response(array_map( function(Employee $employee) {
                return array(
                    'id' => $employee->id()->get(),
                    'name' => $employee->getFullName(),
                    'company' => array(
                        'id' => is_null($employee->company()) ? '' : $employee->company()->id()->get(),
                        'name' => is_null($employee->company()) ? '' : $employee->company()->name()->get(),
                    ),
                    'email' => $employee->getEmail(),
                    'phone' => $employee->getPhone(),
                    'photo' => route('employee.photo.show', $this->randomPhoto())
                );
            }, $employees->get())
        );
    }

    private function randomPhoto()
    {
        $photos =  array(
            'default.jpg',
            'jimrohn.jpg',
            'tonyrobbins.png',
            'melrobbins.jpg'
        );
        return $photos[rand(0, 3)];
    }
}
