<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;

use App\Core\Application\EmployeeService;
use App\Infrastructure\Model\Employee\EmployeeId;

class GetEmployee extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Get Employee Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the retrieving of the employee records,
    | it can return success or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\EmployeeService $service
     *
     * @return void
     */
     public function __construct(EmployeeService $service)
     {
         $this->service = $service;
     }

     /**
     *
     * Handle Getting of employee record
     *
     * @return Illuminate\Http\Response
     */
    public function handle(int $id)
    {
        try
        {
            // Get employee profile otherwise exception is thrown
            $employee = $this->service->getEmployee(
                new EmployeeId($id)
            );

            return response(array(
                'success' => true,
                'data' => array(
                    'id' => $employee->id()->get(),
                    'first_name' => $employee->getFirstName(),
                    'last_name' => $employee->getLastName(),
                    'company' => array(
                        'id' => is_null($employee->company()) ? '' : $employee->company()->id()->get(),
                        'name' => is_null($employee->company()) ? '' : $employee->company()->name()->get(),
                    ),
                    'email' => $employee->getEmail(),
                    'phone' => $employee->getPhone()
                )
            ));
        } catch (\Exception $e) {
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
