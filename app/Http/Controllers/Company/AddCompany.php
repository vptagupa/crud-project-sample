<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Core\Application\CompanyService;
use App\Infrastructure\Model\Company\CompanyName;
use App\Infrastructure\Model\Employee\EmployeeId;
use App\Infrastructure\Common\Email;
use App\Infrastructure\Common\Image;

class AddCompany extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Add Company Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the adding of the company, it can return success
    | or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\CompanyService $service
     *
     * @return void
     */
     public function __construct(CompanyService $service)
     {
         $this->service = $service;
     }

     /**
     *
     * Handle Creating of company
     *
     * @return Illuminate\Http\Response
     */
    public function handle(Request $request)
    {
        try
        {
            // Validate form inputs
            $validated = $request->validate([
                'name' => ['required', 'max:35'],
                'email' => ['required', 'unique:company','email']
            ]);
            // Create company and employees assignment otherwise exception is thrown
            $this->service->create(
                new CompanyName($validated['name']),
                new Email($validated['email']),
                new Image($request->file('photo')),
                array_map( function($id) {
                    return new EmployeeId($id);
                }, explode(",", $request->get('employees')))
            );

            return response(array(
                'success' => true,
                'message' => 'Successfuly Saved!'
            ));
        } catch (\Exception $e) {
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
