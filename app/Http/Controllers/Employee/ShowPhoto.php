<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use File;
use Response;

class ShowPhoto extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Show Employee Logo Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the retrieving of the employee photo,
    | it can return success or error whatever the outcome of the service
    |
    */


    public function handle(string $filename)
    {
       if (!File::exists($path = storage_path('app/photos/' . $filename))) {
         $path = storage_path('app/photo-default.jpg');
       }

       $file = File::get($path);
       $type = File::mimeType($path);

       $response = Response::make($file, 200);
       $response->header("Content-Type", $type);

       return $response;
    }
}
