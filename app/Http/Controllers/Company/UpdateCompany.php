<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Core\Application\CompanyService;
use App\Infrastructure\Model\Company\CompanyName;
use App\Infrastructure\Model\Company\CompanyId;
use App\Infrastructure\Model\Employee\EmployeeId;
use App\Infrastructure\Common\Email;
use App\Infrastructure\Common\Image;

class UpdateCompany extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Update Company Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the updating of the company record,
    | it can return success or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\CompanyService $service
     *
     * @return void
     */
     public function __construct(CompanyService $service, Request $request)
     {
         $this->service = $service;
         $this->request = $request;
     }

     /**
     *
     * Handle Updating of company record
     *
     * @return Illuminate\Http\Response
     */
    public function handle(int $id)
    {
        try
        {
            // Validate form inputs
            $validated = $this->request->validate([
                'name' => ['required', 'max:35'],
                'email' => ['required','email']
            ]);
            // Update company record and employees assignment otherwise exception is thrown
            $this->service->update(
                new CompanyId($id),
                new CompanyName($validated['name']),
                new Email($validated['email']),
                new Image($this->request->file('photo')),
                array_map( function($id) {
                    return new EmployeeId((int)$id);
                }, explode(",", $this->request->get('employees')))
            );

            return response(array(
                'success' => true,
                'message' => 'Successfuly Update!'
            ));
        } catch (\Exception $e) {            
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
