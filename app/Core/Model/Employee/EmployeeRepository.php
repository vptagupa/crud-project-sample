<?php

namespace App\Core\Model\Employee;

use App\Core\Model\Employee\Employee;
use App\Core\Model\Employee\EmployeeId;
use App\Infrastructure\Contract\DataPager;

/**
* This is a Employee repository
* which contains Employee records and events
*/
interface EmployeeRepository
{
    /**
     * Create Employee and return the instance
     *
     * @param Employee $employee
     *
     * @return App\Core\Model\Employee\Employee
     */
    public function createEmployee(Employee $employee): Employee;

    /**
     * Update Employee
     *
     * @param App\Core\Model\Employee\Employee $employee
     *
     * @return void
     */
    public function updateEmployee(Employee $employee): void;

    /**
     * Delete Employee
     *
     * @param App\Core\Model\Employee\EmployeeId $employeeId
     *
     * @return bool
     */
    public function deleteEmployee(EmployeeId $employeeId): bool;

    /**
     * Get List of employees with limit and offset
     * By a given like criteria
     *
     * @param array $likeCriteria
     * @param int $limit
     * @param int $offset
     *
     * @return App\Infrastructure\Contract\DataPager
     */
    public function getEmployees(
        array $likeCriteria = array(),
        int $limit = 10,
        int $offset = 0
    ): DataPager;

    /**
     * Get employee record
     * By a given id
     *
     * @param App\Core\Model\Employee\EmployeeId $employeeId
     *
     * @return Employee
     */
    public function getEmployee(EmployeeId $employeeId): ?Employee;
}
