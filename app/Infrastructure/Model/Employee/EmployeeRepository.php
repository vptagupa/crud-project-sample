<?php

namespace App\Infrastructure\Model\Employee;

use App\Infrastructure\Model\Employee\Employee;
use App\Infrastructure\Model\Employee\EmployeeId;
use App\Infrastructure\Model\Employee\EmployeeName;
use App\Infrastructure\Model\Company\Company;
use App\Infrastructure\Model\Company\CompanyId;
use App\Infrastructure\Model\Company\CompanyName;

use App\Infrastructure\Contract\DataPager as DataPagerInterface;
use App\Infrastructure\Common\DataPager;
use App\Infrastructure\Common\Email;

use App\Core\Model\Employee\EmployeeRepository as EmployeeRepositoryInterface;

use App\Core\Model\Employee\Employee as EmployeeInterface;
use App\Core\Model\Employee\EmployeeId as EmployeeIdInterface;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* This is a Employee repository
* which contains Employee records and events
*/
final class EmployeeRepository extends Model implements EmployeeRepositoryInterface
{
    /**
    * Use soft delete
    * to prevent deletion the records from the databse
    *
    */
    use SoftDeletes;

    /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'employee';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'company_id',
        'email','phone'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Create Employee and return the instance
     *
     * @param App\Core\Model\Employee\Employee $employee
     *
     * @return App\Core\Model\Employee\Employee
     */
    public function createEmployee(EmployeeInterface $employee): EmployeeInterface
    {
        return $this->employee($this->create(array(
            'first_name' => $employee->getFirstName(),
            'last_name' => $employee->getLastName(),
            'email' => $employee->getEmail(),
            'phone' => $employee->getPhone()
        )));
    }

    /**
     * Update Employee
     *
     * @param App\Core\Model\Employee\Employee $employee
     *
     * @return void
     */
    public function updateEmployee(EmployeeInterface $employee): void
    {
        $this->where('id', $employee->id()->get())
            ->update(array(
                'first_name' => $employee->getFirstName(),
                'last_name' => $employee->getLastName(),
                'email' => $employee->getEmail(),
                'company_id' => $employee->company()->id()->get(),
                'phone' => $employee->getPhone()
            ));
    }

    /**
     * Delete Employee
     *
     * @param App\Core\Model\Employee\EmployeeId $employeeId
     *
     * @return bool
     */
    public function deleteEmployee(EmployeeIdInterface $employeeId): bool
    {
        return $this->where('id', $employeeId->get())->delete();
    }

    /**
     * Get employee record
     * By a given id
     *
     * @param App\Core\Model\Employee\EmployeeId $employeeId
     *
     * @return Employee
     */
    public function getEmployee(EmployeeIdInterface $employeeId): ?EmployeeInterface
    {
        $employee = $this->select(array(
            'employee.id','first_name','last_name','employee.email',
            'phone','company.name as company_name',
            'company_id','company.email as company_email'
        ))->leftJoin('company',
         'company.id','=','company_id'
        )->find($employeeId->get());

        if (empty($employee)) {
            return null;
        }
        return $this->employee($employee);
    }

    /**
     * Get List of employees with limit and offset
     * By a given like criteria
     *
     * @param array $likeCriteria
     * @param int $limit
     * @param int $offset
     *
     * @return App\Infrastructure\Contract\DataPager
     */
    public function getEmployees(
        array $likeCriteria = array(),
        int $limit = 10,
        int $offset = 0
    ): DataPagerInterface {
        $model = $this;
        $model_ = $model;
        $model = $this->select(array(
            'employee.id','first_name','last_name','employee.email',
            'phone','company.name as company_name',
            'company_id','company.email as company_email'
        ))->leftJoin('company',
             'company.id','=','company_id'
        );
        // Set query where clause for input query
        foreach($likeCriteria as $key => $value) {
            if (!empty($value) && !empty($key)) {
                if (strtolower($key) === 'name') {
                    $model->where( function($query) use($value) {
                        $query->Orwhere('first_name','like', '%'.$value.'%');
                        $query->Orwhere('last_name','like', '%'.$value.'%');
                    });
                } else {
                    $model = $model->where($key,'like', '%'.$value.'%');
                }
            }
        }

        $model = $model->limit($limit)->offset($offset);
        $collection = collect();
        foreach($model->get() as $employee) {
            $collection->push(
                $this->employee($employee)
            );
        }

        return new DataPager($model_->count(), $collection);
    }

    /**
     * Get Employee Instance
     *
     * @param Collection $employee
     *
     * @return App\Core\Model\Employee\Employee
     */
    private function employee($employee): EmployeeInterface
    {
        return new Employee(
            new EmployeeId($employee->id),
            new EmployeeName($employee->first_name, $employee->last_name),
            new Company(
                new CompanyId($employee->company_id ??  0),
                new CompanyName($employee->company_name ?? ''),
                new Email($employee->company_email ?? ''),
                '',
                array()
            ),
            new Email($employee->email),
            $employee->phone
        );
    }
}
