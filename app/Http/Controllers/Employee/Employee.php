<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;


class Employee extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | View Employee Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the employee page,
    |
    */

    /**
     * Create a new controller instance.
     *
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth');
     }

     public function index()
     {
         return view('employee.index');
     }
}
