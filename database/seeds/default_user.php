<?php

use Illuminate\Database\Seeder;

class default_user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'name' => 'Vic Tagupa',
                'password' => bcrypt('secret'),
                'email' => 'dev@gmail.com',
                'role' => 'admin'
            )
        );

        foreach($data as $user) {
            if (DB::table('users')->where('email',$user['email'])->count() <= 0) {
                DB::table('users')->insert($user);
            }
        }
    }
}
