import izitoast from "izitoast";
import "izitoast/dist/css/iziToast.css";

const success = message => {
    izitoast.success({
        title: "Success!",
        message: message,
        position: "topRight"
    });
};

const error = message => {
    izitoast.error({
        title: "Error!",
        message: message,
        position: "topRight"
    });
};

const info = message => {
    izitoast.info({
        title: "Info!",
        message: message,
        position: "topRight"
    });
};

const question = (title, message, callback) => {
    izitoast.question({
        title: title,
        message: message,
        position: "center",
        progressBar: false,
        close: false,
        buttons: [
            [
                "<button><b>YES</b></button>",
                function(instance, toast) {
                    callback();
                    instance.hide(
                        { transitionOut: "fadeOut" },
                        toast,
                        "button"
                    );
                },
                true
            ],
            [
                "<button>NO</button>",
                function(instance, toast) {
                    instance.hide(
                        { transitionOut: "fadeOut" },
                        toast,
                        "button"
                    );
                }
            ]
        ],
        onClosing: function(instance, toast, closedBy) {
            // console.info('Closing | closedBy: ' + closedBy);
        },
        onClosed: function(instance, toast, closedBy) {
            // console.info('Closed | closedBy: ' + closedBy);
        }
    });
};

export { success, error, info, question };
