<?php

namespace App\Core\Model\Company;

use App\Core\Model\Company\CompanyName;
use App\Core\Model\Company\CompanyId;
use App\Infrastructure\Contract\Email;

/**
* This is a company interface that defines its attributes
*/
interface Company
{
    /**
     * Get company id
     *
     * @return App\Core\Model\Company\CompanyId
     */
    public function id(): CompanyId;


    /**
     * Get company name
     *
     * @return App\Core\Model\Company\CompanyName
     */
    public function name(): CompanyName;

    /**
     * Get company logo
     *
     * @return string
     */
    public function getLogo(): string;

    /**
     * Change company name
     *
     * @param App\Core\Model\Company\CompanyName $name
     *
     * @return void
     */
    public function changeName(CompanyName $name): void;

    /**
     * Change company email
     *
     * @param Email $email
     *
     * @return void
     */
    public function changeEmail(Email $email): void;

    /**
     * Change company logo
     *
     * @param string $logo
     *
     * @return void
     */
    public function changeLogo(string $logo): void;
}
