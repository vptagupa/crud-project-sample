<?php

namespace App\Infrastructure\Model\Employee;

use App\Core\Model\Employee\EmployeeName as EmployeeNameInterface;

/**
* This is Employee Name as a value object
* This will protect the Employee name on its desired value
*/
class EmployeeName implements EmployeeNameInterface
{
    /**
    *  First name
    * @var $firstName
    */
    private $firstName;

    /**
    *  last name
    * @var $lastName
    */
    private $lastName;

    /**
    * Initiate employee name
    *
    * @param string $firstName
    * @param string $lastName
    */
    public function __construct(string $firstName, string $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * Get employee Full Name
     *
     * @return string
     */
    public function getFullName(): string
    {
        return $this->firstName.' '.$this->lastName;
    }

    /**
     * Get employee First Name
     *
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * Get employee Last Name
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
    * Get employee full name
    * @return string
    */
    public function __toString(): string
    {
        return $this->getFullName();
    }
}
