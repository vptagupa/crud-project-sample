<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Core\Application\EmployeeService;
use App\Core\Model\Employee\Employee;

class ListEmployee extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | List Employee Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the listing of the employees,
    | it can return success or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\EmployeeService $service
     *
     * @return void
     */
     public function __construct(EmployeeService $service)
     {
         $this->service = $service;
     }

     /**
     *
     * Handle Listing of employees
     *
     * @return Illuminate\Http\Response
     */
    public function handle(Request $request)
    {
        try
        {
            // List employees with pagination and search criteria
            $employees = $this->service->listEmployee(
                $request->get('criteria'),
                $request->get('limit'),
                ($request->get('page') - 1) * $request->get('limit')
            );

            return response(array(
                'success' => true,
                'total_rows' => $employees->getTotalRecords(),
                'data' => array_map( function(Employee $employee) {
                    return array(
                        'id' => $employee->id()->get(),
                        'name' => $employee->getFullName(),
                        'company' => array(
                            'id' => is_null($employee->company()) ? '' : $employee->company()->id()->get(),
                            'name' => is_null($employee->company()) ? '' : $employee->company()->name()->get(),
                        ),
                        'email' => $employee->getEmail(),
                        'phone' => $employee->getPhone()
                    );
                }, $employees->get())
            ));
        } catch (\Exception $e) {
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
