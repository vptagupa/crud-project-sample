<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function($route) {
    $route->get('/login', 'Auth\Admin@index')->name('login');
    $route->post('/login', 'Auth\Admin@login')->name('auth.admin.login');
    $route->get('/logout', 'Auth\Admin@logout')->name('auth.admin.logout');
});

Route::group(['prefix' => 'company'], function($route) {
    $route->get('/', 'Company\Company@index')->name('company');
    $route->post('/add', 'Company\AddCompany@handle')->name('company.add');
    $route->post('/update/{id}', 'Company\UpdateCompany@handle')->name('company.update');
    $route->delete('/delete/{id}', 'Company\DeleteCompany@handle')->name('company.delete');
    $route->get('/get/{id}', 'Company\GetCompany@handle')->name('company.get');
    $route->post('/list', 'Company\ListCompany@handle')->name('company.list');
    $route->get('/storage/logo/{filename}', 'Company\ShowLogo@handle')->name('company.logo.show');
    $route->get('/employee/search/{name}', 'Company\SearchEmployee@handle')->name('company.employee.search');
});

Route::group(['prefix' => 'employee'], function($route) {
    $route->get('/', 'Employee\Employee@index')->name('employee');
    $route->put('/add', 'Employee\AddEmployee@handle')->name('employee.add');
    $route->patch('/update/{id}', 'Employee\UpdateEmployee@handle')->name('employee.update');
    $route->delete('/delete/{id}', 'Employee\DeleteEmployee@handle')->name('employee.delete');
    $route->get('/get/{id}', 'Employee\GetEmployee@handle')->name('employee.get');
    $route->post('/list', 'Employee\ListEmployee@handle')->name('employee.list');
    $route->get('/company-relation/list', 'Employee\ListCompanyRelation@handle')->name('employee.relations');
    $route->get('/storage/photo/{filename}', 'Employee\ShowPhoto@handle')->name('employee.photo.show');
    $route->post('/import', 'Employee\ImportEmployee@handle')->name('employee.list');
});
