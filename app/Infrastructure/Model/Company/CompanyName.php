<?php

namespace App\Infrastructure\Model\Company;

use App\Core\Model\Company\CompanyName as CompanyNameInterface;

/**
* This is a Company Name as a value object
* This will protect the company name on its desired value
*/
class CompanyName implements CompanyNameInterface
{

    /**
    *  Company name
    * @var $name
    */
    private $name;

    /**
    * Initiate company name
    *
    * @param string $firstName
    *
    * @return void
    */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Get company name
     *
     * @return string
     */
    public function get(): string
    {
        return $this->name;
    }

    /**
    * Get company name
    * @return string
    */
    public function __toString(): string
    {
        return $this->name;
    }
}
