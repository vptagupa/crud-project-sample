<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;

class Admin extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Admin authentiction Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles of the admin page authentication,
    |
    */

    const ROLE_ADMIN = 'admin';

    /**
     * Create a new controller instance.
     *
     *
     * @return void
     */
     public function __construct()
     {

     }

     public function index()
     {
         return view('login');
     }

     public function logout()
     {
         $this->guard()->logout();
         return redirect('/');
     }

     public function login(Request $request)
     {
         $credentials = $this->credentials($request);
         if ($this->guard()->attempt($credentials, $request->has('remember'))) {
             if (strtolower(Auth::user()->role) === self::ROLE_ADMIN) {
                  return $this->sendLoginResponse($request);
             } else {
                 $this->guard()->logout();
             }
         }

         return $this->sendFailedLoginResponse($request);
     }

     /**
      * Send the response after the user was authenticated.
      *
      * @return \Illuminate\Http\Response
      */
     protected function sendLoginResponse(Request $request)
     {
         return [
             'success' => true,
             'message' => 'Success!'
         ];
     }

     /**
      * Get the failed login response instance.
      *
      * @param \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     protected function sendFailedLoginResponse(Request $request)
     {
          return [
             'success' => false,
             'message' => Lang::get('auth.failed')
         ];
     }

      /**
      * Get the needed authorization credentials from the request.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return array
      */
     protected function credentials(Request $request)
     {
         return $request->only($this->username(), 'password');
     }

     /**
      * Get the login username to be used by the controller.
      *
      * @return string
      */
     public function username()
     {
         return 'email';
     }

     /**
      * Get the guard to be used during authentication.
      *
      * @return \Illuminate\Contracts\Auth\StatefulGuard
      */
     protected function guard()
     {
         return Auth::guard();
     }
}
