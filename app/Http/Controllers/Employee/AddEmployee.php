<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Core\Application\EmployeeService;
use App\Infrastructure\Model\Employee\EmployeeName;
use App\Infrastructure\Common\Email;

class AddEmployee extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Add Employee Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the adding of the employee,
    | it can return success or error whatever the outcome of the service
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param App\Core\Application\EmployeeService $service
     *
     * @return void
     */
     public function __construct(EmployeeService $service)
     {
         $this->service = $service;
     }

     /**
     *
     * Handle Adding of new employee
     *
     * @return Illuminate\Http\Response
     */
    public function handle(Request $request)
    {
        try
        {
            // Validate form inputs
            $validated = $request->validate([
                'first_name' => ['required', 'max:35'],
                'last_name' => ['required', 'max:35'],
                'email' => ['unique:employee','email']
            ]);

            // Create employee otherwise exception is thrown
            $this->service->create(
                new EmployeeName($validated['first_name'], $validated['last_name']),
                new Email($validated['email']),
                $request->get('phone')
            );

            return response(array(
                'success' => true,
                'message' => 'Successfuly Saved!'
            ));
        } catch (\Exception $e) {
            return response(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
