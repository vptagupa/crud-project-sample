<?php

namespace App\Infrastructure\Common;

class Curl
{
    /**
    *
    * var string
    */
    private $url;

    public function __construct(string $url) {
        $this->url = $url;
    }

    /**
    *
    * Execute curl to retrieve content
    *
    */
    public function get()
    {
        $ch = curl_init($this->url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $data = curl_exec($ch);

        curl_close($ch);

        return $data;
    }
}
