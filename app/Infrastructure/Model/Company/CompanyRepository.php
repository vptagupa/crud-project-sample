<?php

namespace App\Infrastructure\Model\Company;

use App\Infrastructure\Model\Company\Company;
use App\Infrastructure\Model\Company\CompanyName;
use App\Infrastructure\Model\Company\CompanyId;
use App\Infrastructure\Model\Employee\Employee;
use App\Infrastructure\Model\Employee\EmployeeId;
use App\Infrastructure\Model\Employee\EmployeeName;

use App\Core\Model\Company\CompanyRepository as CompanyRepositoryInterface;

use App\Infrastructure\Contract\DataPager as DataPagerInterface;
use App\Infrastructure\Common\DataPager;
use App\Infrastructure\Common\Email;

use App\Core\Model\Company\Company as CompanyInterface;
use App\Core\Model\Company\CompanyId as CompanyIdInterface;

use DB;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
* This is a company repository
* which contains company records and events
*/
final class CompanyRepository extends Model implements CompanyRepositoryInterface
{
    /**
    * Use soft delete
    * to prevent deletion the records from the databse
    *
    */
    use SoftDeletes;

    /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'company';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'logo'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Create Company
     *
     * @param App\Core\Model\Company\Company $company
     *
     * @return void
     */
    public function createCompany(CompanyInterface $company): CompanyInterface
    {
        return $this->company($this->create(array(
            'name' => $company->name()->get(),
            'email' => $company->getEmail(),
            'logo' => $company->getLogo()
        )), array());
    }

    /**
     * Update Company
     *
     * @param App\Core\Model\Company\Company $company
     *
     * @return void
     */
    public function updateCompany(CompanyInterface $company): void
    {
        $this->where('id', $company->id()->get())
            ->update(array(
                'name' => $company->name()->get(),
                'email' => $company->getEmail(),
                'logo' => $company->getLogo()
            ));
    }

    /**
     * Delete Company
     *
     * @param App\Core\Model\Company\CompanyId $companyId
     *
     * @return bool
     */
    public function deleteCompany(CompanyIdInterface $companyId): bool
    {
        return $this->where('id', $companyId->get())->delete();
    }

    /**
     * Get company record
     * By a given id
     *
    * @param App\Core\Model\Company\CompanyId $companyId
     *
     * @return App\Core\Model\Company\Company
     */
    public function getCompany(CompanyIdInterface $companyId): ?CompanyInterface
    {
        $company = $this->leftJoin('employee',
            'employee.company_id','=','company.id'
        )
        ->select(array(
            'company.*',
            'employee.first_name',
            'employee.last_name',
            'employee.id as employee_id',
            'employee.phone as employee_phone'
        ))
        ->where('company.id',$companyId->get())
            ->get();

        if (! $company) {
            return null;
        }
        $employees = collect();
        foreach($company as $employee) {
            if (! empty($employee->employee_id)) {
                $employees->push(
                    new Employee(
                        new EmployeeId($employee->employee_id),
                        new EmployeeName($employee->first_name, $employee->last_name),
                        null,
                        new Email($employee->email),
                        $employee->employee_phone
                    )
                );
            }

        }
    
        return $this->company($company[0], $employees->all());
    }

    /**
     * Clear Company Employees
     *
     * @param App\Core\Model\Company\CompanyId $companyId
     *
     * @return void
     */
    public function clearEmployees(CompanyIdInterface $companyId): void
    {
        DB::table('employee')->where('company_id', $companyId->get())
            ->update(array(
                'company_id' => 0
            ));
    }

    /**
     * Get List of Companies with limit and offset
     * By a given like criteria
     *
     * @param array $likeCriteria
     * @param int $limit
     * @param int $offset
     *
     * @return App\Infrastructure\Contract\DataPager
     */
    public function getCompanies(
        array $likeCriteria = array(),
        int $limit = 10,
        int $offset = 0
    ): DataPagerInterface
    {
        $model = $this;
        $model_ = $model;
        $model = $model->select('*');

        foreach($likeCriteria as $key => $value) {
            if (!empty($value) && !empty($key)) {
                $model = $model->where($key,'like', '%'.$value.'%');
            }
        }

        $model = $model->limit($limit)->offset($offset);
        $collection = collect();
        foreach($model->get() as $company) {
            $collection->push(
                new Company(
                    new CompanyId($company->id),
                    new CompanyName($company->name),
                    new Email($company->email),
                    $company->logo,
                    array()
                )
            );
        }

        return new DataPager($model_->count(), $collection);
    }

    /**
     * Get company employees list
     * By a given id
     *
    * @param App\Core\Model\Company\CompanyId $companyId
     *
     * @return Collection
     */
    public function getEmployees(CompanyIdInterface $companyId): Collection
    {
        $company = $this->leftJoin('employee',
            'employee.company_id','=','company.id'
        )
        ->select(array(
            'company.*',
            'employee.first_name',
            'employee.last_name',
            'employee.id as employee_id',
            'employee.phone as employee_phone'
        ))
        ->where('company.id',$companyId->get())
            ->get();

        if (! $company) {
            return null;
        }
        $employees = collect();
        foreach($company as $employee) {
            $employees->push(
                new Employee(
                    new EmployeeId($employee->employee_id),
                    new EmployeeName($employee->first_name, $employee->last_name),
                    null,
                    new Email($employee->email),
                    $employee->employee_phone
                )
            );
        }
        return $employees;
    }

    /**
     * Get Company Instance
     *
     * @param Collection $company
     * @param array $employees
     *
     * @return App\Core\Model\Company\Company
     */
    private function company($company, array $employees): CompanyInterface
    {
        return new Company(
            new CompanyId($company->id),
            new CompanyName($company->name),
            new Email($company->email),
            $company->logo,
            $employees
        );
    }
}
