<?php

namespace App\Core\Model\Company;

/**
* This is a Company id as a value object
*/
interface CompanyId
{
    /**
     * Get company id
     *
     * @return int
     */
    public function get(): int;
}
