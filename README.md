## About

This project is a laravel and vue framework.
It can create, update, delete and search employees and companies
It can assign employees to company
It can import list of users to employees via api end point

## Project Installation

Execute the commands below

```
composer install
npm install
php artisan migrate
php artisan db:seed
npm run development
```

Login Details

```
Login
email: dev@gmail.com
password: secret
```

## License

The Laravel and Vue framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
