<?php

namespace App\Infrastructure\Contract;

interface DataPager {

     /**
     *
     * Get Total Records
     * @return int
     */
     public function getTotalRecords(): int;

     /**
     *
     * Get Records
     * @return int
     */
     public function get(): array;
 }
