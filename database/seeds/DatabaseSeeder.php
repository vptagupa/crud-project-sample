<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(default_user::class);
        // $this->call(insert_employee::class);
        $this->call(insert_company::class);
    }
}
